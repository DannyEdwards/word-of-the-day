const CACHE_VERSION = "2.11.0";
const CACHE_KEY = `word-ot-day-v${CACHE_VERSION}`;
const staticAssets = [
  "index.html",
  "main.js",
  "main.css",
  "manifest.json",
  "icon/192.png",
  "icon/512.png",
];

self.addEventListener("install", (event) => {
  event.waitUntil(
    (async () => {
      const cache = await caches.open(CACHE_KEY);
      await cache.addAll(staticAssets);
    })(),
  );
});

self.addEventListener("fetch", (event) => {
  event.respondWith(
    (async () => {
      const cache = await caches.open(CACHE_KEY);
      const cachedResponse = await cache.match(event.request);
      if (cachedResponse && !navigator.onLine) return cachedResponse;
      if (cachedResponse?.type === "basic" && navigator.onLine)
        return cachedResponse;
      // @note We're only going to fetch if there's no cache, we're offline, and
      // the request is a cross-origin resource (not "basic").
      try {
        const response = await fetch(event.request);
        cache.put(event.request, response.clone());
        return response;
      } catch (error) {
        if (!cachedResponse) throw error;
        return cachedResponse;
      }
    })(),
  );
});

self.addEventListener("activate", (event) => {
  event.waitUntil(
    (async () => {
      const cacheKeys = await caches.keys();
      return cacheKeys.map((key) => key !== CACHE_KEY && caches.delete(key));
    })(),
  );
});
