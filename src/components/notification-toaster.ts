export const ERROR_MESSAGE = "⚠️ There has been an error.";
export const OFFLINE_MESSAGE = "⚠️ Application offline.";

export default class NotificationToaster extends HTMLElement {
  constructor(rootEl: HTMLElement, isHidden?: boolean) {
    super();
    if (isHidden) this.setAttribute("hidden", "");
    rootEl.append(this);
  }
  connectedCallback() {
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.innerHTML = this.render();
  }
  show(message: string) {
    this.removeAttribute("hidden");
    if (message) this.textContent = message;
  }
  hide() {
    this.setAttribute("hidden", "");
    this.textContent = "";
  }
  protected render() {
    return /*html*/ `
      <style>
        :host {
          align-items: center;
          background: hsla(1, 0%, 0%, 0.5);
          bottom: 0;
          display: flex;
          font-size: 0.5em;
          height: 2em;
          justify-content: center;
          position: fixed;
          transition: bottom 0.2s ease-out;
          width: 100%;
        }
        :host([hidden]) {
          bottom: -2em;
        }
      </style>
      <p>
        <slot></slot>
      </p>
    `;
  }
}

customElements.define("notification-toaster", NotificationToaster);
