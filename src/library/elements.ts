import Notification from "../components/notification-toaster.ts";

export const bodyEl = document.querySelector("body")!;
export const mainEl = document.querySelector("main")!;
export const loaderEl = document.querySelector(".loader")!;
export const notificationEl = new Notification(bodyEl, true);
