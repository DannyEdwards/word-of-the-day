import { PROXY_URL } from "../../env.ts";

const OED_URL = "https://www.oed.com/rss/wordoftheday";

export default async function getWords() {
  const response = await fetch(`${PROXY_URL}${OED_URL}`);
  if (!response.ok) throw "⚠️ Connection to OED was unsuccessful.";
  const textXML = await response.text();
  const DOMFromString = new DOMParser().parseFromString(textXML, "text/xml");
  return [...DOMFromString.querySelectorAll("item")].map((itemEl) => {
    const { heading, link, description, pubDate } = getPropsFromItemEl(itemEl);
    if (!heading || !link || !description || !pubDate)
      throw new SyntaxError("Malformed XML response.");
    const [specialDay, definition] = splitDescription(description);
    return {
      heading,
      link,
      date: new Date(pubDate),
      specialDay,
      definition: definition.replace(heading, "").trim(),
    };
  });
}

function getPropsFromItemEl(element: Element) {
  return {
    heading: element.querySelector("title")?.textContent,
    link: element.querySelector("link")?.textContent,
    description: element.querySelector("description")?.textContent,
    pubDate: element.querySelector("pubDate")?.textContent,
  };
}

function splitDescription(description: string) {
  const TITLE = "OED Word of the Day";
  // @note Some descriptions contain non-breaking spaces.
  return description.normalize("NFKD").replace(TITLE, "").split(/:(.+)/);
}
